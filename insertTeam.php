<?php
	include('session.php');

	if(isset($_POST['league']) && isset($_POST['tname'])) {
		$league = $_POST['league'];
		$tname = $_POST['tname'];

		if($_FILES['image']['name']) {
			$trim_logo_name = preg_replace('/\s+/','', $_FILES['image']['name']);
			$logo_filepath = " ../testFolder/".$league."/";
			$logo_filename = strtolower($trim_logo_name);
			move_uploaded_file($_FILES['image']['tmp_name'], $logo_filepath.$logo_filename);
		}

		$sql = "INSERT INTO tbl_team (league, team_name, logo_filepath, logo_filename) VALUES ('$league','$tname','$logo_filepath','$logo_filename')";
		mysqli_query($db, $sql);
		
		$user = $_SESSION['user_id'];
		$transaction = "Insert Team - ".$league.": ".$tname;
		$log = "INSERT INTO tbl_activity_log (transaction, user_id) VALUES ('$transaction', '$user');";
		mysqli_query($db, $log);

	} else {
		echo "Error!";
	}

	header("location: add_team.php");
?>