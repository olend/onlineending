<!DOCTYPE html>
<?php
	include('session.php');
?>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.3
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
		
		<!--PAGE TITLE-->
        <title>Add Match Up</title>
        
		
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
		<link href="assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout2/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout2/css/themes/blue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout2/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
			
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="admin_dashboard.php">
                        <img src="assets/layouts/layout2/img/ed_logo_a.png" alt="logo" class="logo-default" />
					</a>
                </div>
                <!-- END LOGO -->
				
                <!-- BEGIN PAGE ACTIONS -->
                <!-- DOC: Remove "hide" class to enable the page header actions -->
                <div class="page-actions">
                    <div class="btn-group">
                        <button type="button" class="btn btn-circle btn-outline red dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-plus"></i>&nbsp;
                            <span class="hidden-sm hidden-xs">New&nbsp;</span>&nbsp;
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="add_team.php">
                                    <i class="icon-social-dribbble"></i> Team </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-trophy"></i> Match Up </a>
                            </li>
                            <li>
                                <a href="add_admin.php">
                                    <i class="icon-support"></i> Administrator </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- END PAGE ACTIONS -->
				
                <!-- BEGIN PAGE TOP -->
                <div class="page-top">
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
						
                            <!-- BEGIN TODO DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-extended dropdown-tasks" id="header_task_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-calendar"></i>
                                    <span class="badge badge-default"> 3 </span>
                                </a>
                                <ul class="dropdown-menu extended tasks">
                                    <li class="external">
                                        <h3>You have
                                            <span class="bold">12 pending</span> tasks</h3>
                                        <a href="javascript:;">view all</a>
                                    </li>
                                    <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="task">
                                                        <span class="desc">Web server upgrade</span>
                                                        <span class="percent">58%</span>
                                                    </span>
                                                    <span class="progress">
                                                        <span style="width: 58%;" class="progress-bar progress-bar-info" aria-valuenow="58" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">58% Complete</span>
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="task">
                                                        <span class="desc">Mobile development</span>
                                                        <span class="percent">85%</span>
                                                    </span>
                                                    <span class="progress">
                                                        <span style="width: 85%;" class="progress-bar progress-bar-success" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">85% Complete</span>
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="task">
                                                        <span class="desc">New UI release</span>
                                                        <span class="percent">38%</span>
                                                    </span>
                                                    <span class="progress progress-striped">
                                                        <span style="width: 38%;" class="progress-bar progress-bar-important" aria-valuenow="18" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">38% Complete</span>
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <!-- END TODO DROPDOWN -->
							
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="assets/layouts/layout2/img/avatar_la_sm.png" />
									<?php
										//RETRIEVE USER AVATAR HERE
									?>
                                    <!--<span class="username username-hide-on-mobile"> Leslie </span>-->
                                    <span class="username username-hide-on-mobile">
										<?php
											echo $_SESSION['uname'];
										?>
									</span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="admin_profile.php">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
                                    <li>
                                        <a href="app_calendar.html">
                                            <i class="icon-calendar"></i> My Calendar </a>
                                    </li>
                                    <li>
                                        <a href="logout.php">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
							
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END PAGE TOP -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
		
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <li class="nav-item start ">
                            <a href="admin_profile.php" class="nav-link nav-toggle">
                                <i class="icon-user"></i>
                                <span class="title">Profile</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-social-dribbble"></i>
                                <span class="title">Teams</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="add_team.php" class="nav-link ">
                                        <span class="title">Add</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="delete_team.php" class="nav-link ">
                                        <span class="title">Delete</span>
                                    </a>
                                </li>
							</ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-trophy"></i>
                                <span class="title">Match Ups</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="javascript:;" class="nav-link ">
                                        <span class="title">Add</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="update_match.php" class="nav-link ">
                                        <span class="title">Update</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="delete_match.php" class="nav-link ">
                                        <span class="title">Delete</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-support"></i>
                                <span class="title">Administrators</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="add_admin.php" class="nav-link ">
                                        <span class="title">Add</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="delete_admin.php" class="nav-link ">
                                        <span class="title">Delete</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="view_allMembers.php" class="nav-link nav-toggle">
                                <i class="icon-users"></i>
                                <span class="title">View Members</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="view_allLogs.php" class="nav-link nav-toggle">
                                <i class="icon-book-open"></i>
                                <span class="title">View Logs</span>
                            </a>
                        </li>
                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
			
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title" style="font-weight:bold; padding:0px 20px 0px 20px;"> MATCH UPS </h3>
                    <div class="row">
                        <div class="col-md-12">
							
							<!-- START TAB PORTLET -->
							<div class="portlet light portlet-fit portlet-form">
								<div class="portlet-title tabbable-line">
									<div class="caption">
										<i class="glyphicon glyphicon-plus font-red"></i>
										 <span class="caption-subject font-red bold uppercase">Add</span>
									</div>
									<ul class="nav nav-tabs">
										<li>
											<a href="#portlet_tab2" data-toggle="tab" style="font-size: 20px; font-weight:bold; padding:0px 20px 0px 20px;"> NBA </a>
										</li>
										<li class="active">
											<a href="#portlet_tab1" data-toggle="tab" style="font-size: 20px; font-weight:bold; padding:0px 20px 0px 20px;"> PBA </a>
										</li>
									</ul>
								</div>
								<div class="portlet-body">
								
									<div class="tab-content">
										<!--START PBA TAB-->
										
										<div class="tab-pane active" id="portlet_tab1">
											<form action="insertMatchPBA.php" method="POST" class="form-horizontal form-bordered">
												<div class="form-body">
												
													<!-- START TEAM 1 NAME -->
													<div class="form-group">
														<label class="control-label col-md-4"> Team 1
															<span class="required"> * </span>
														</label>
														<div class="col-md-4">
															<select class="form-control" name="team1">
																<?php
																	$sql = "SELECT team_name FROM tbl_team WHERE league = 'PBA'";
																	$result = mysqli_query($db, $sql);
																	$row = mysqli_num_rows($result);
																	while($row = mysqli_fetch_array($result)) {
																		echo "<option value='".$row['team_name']."'>".$row['team_name']."</option>";
																	}
																?>
															</select>
														</div>
													</div>
													<!-- END TEAM 1 NAME -->

													<!-- START TEAM 2 NAME -->
													<div class="form-group">
														<label class="control-label col-md-4"> Team 2
														<span class="required"> * </span>
														</label>
														<div class="col-md-4">
															<select class="form-control" name="team2">
																<?php
																	$sql = "SELECT team_name FROM tbl_team WHERE league = 'PBA'";
																	$result = mysqli_query($db, $sql);
																	$row = mysqli_num_rows($result);
																	while($row = mysqli_fetch_array($result)) {
																		echo "<option value='".$row['team_name']."'>".$row['team_name']."</option>";
																	}
																?>
															</select>
														</div>
													</div>
													<!-- END TEAM 2 NAME -->

													<!-- START MATCH DATE -->
													<div class="form-group">
														<label class="control-label col-md-4">Match Date
														<span class="required"> * </span>
														</label>
														<div class="col-md-4">
															<div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
																<input type="text" class="form-control" name="dp_date_p">
																<span class="input-group-btn">
																	<button class="btn default" type="button">
																		<i class="fa fa-calendar"></i>
																	</button>
																</span>
															</div>
														</div>
													</div>
													<!-- END MATCH DATE -->
													
													<!-- START MATCH TIME -->
													<div class="form-group">
														<label class="control-label col-md-4">Match Time
															<span class="required"> * </span>
														</label>
														<div class="col-md-4">
															<div class="input-group">
																<input type="text" class="form-control timepicker timepicker-no-seconds" name="tp_time_p">
																<span class="input-group-btn">
																	<button class="btn default" type="button">
																		<i class="fa fa-clock-o"></i>
																	</button>
																</span>
															</div>
														</div>
													</div>
													<!-- END MATCH TIME -->
													
													<div class="form-actions">
														<div class="row">
															<div class="col-md-offset-5 col-md-7">											
																<!-- Button to trigger modal -->
																<a href="#myModal2" role="button" class="btn green" data-toggle="modal"> Add </a>
															</div>
														</div>
													</div>
													<!-- Modal -->
													<div id="myModal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
																	<h4 class="modal-title">Confirm Action</h4>
																</div>
																<div class="modal-body">
																	<p> You are about to add a new PBA Match Up. Please confirm action. </p>
																</div>
																<div class="modal-footer">
																	<input type="submit" class="btn green" value="&nbsp;Confirm&nbsp;">
																</div>
															</div>
														</div>
													</div>
													
												
												</div>
											</form>
										
										</div>
										<!--END PBA TAB-->
										
										<!--START NBA TAB-->
										<div class="tab-pane" id="portlet_tab2">
											
											<form action="insertMatchNBA.php" method="POST" class="form-horizontal form-bordered">
												<div class="form-body">
												
													<!-- START TEAM 1 NAME -->
													<div class="form-group">
														<label class="control-label col-md-4"> Team 1
															<span class="required"> * </span>
														</label>
														<div class="col-md-4">
															<select class="form-control" name="team3">
																<?php
																	$sql = "SELECT team_name FROM tbl_team WHERE league = 'NBA'";
																	$result = mysqli_query($db, $sql);
																	$row = mysqli_num_rows($result);
																	while($row = mysqli_fetch_array($result)) {
																		echo "<option value='".$row['team_name']."'>".$row['team_name']."</option>";
																	}
																?>
															</select>
														</div>
													</div>
													<!-- END TEAM 1 NAME -->

													<!-- START TEAM 2 NAME -->
													<div class="form-group">
														<label class="control-label col-md-4"> Team 2
														<span class="required"> * </span>
														</label>
														<div class="col-md-4">
															<select class="form-control" name="team4">
																<?php
																	$sql = "SELECT team_name FROM tbl_team WHERE league = 'NBA'";
																	$result = mysqli_query($db, $sql);
																	$row = mysqli_num_rows($result);
																	while($row = mysqli_fetch_array($result)) {
																		echo "<option value='".$row['team_name']."'>".$row['team_name']."</option>";
																	}
																?>
															</select>
														</div>
													</div>
													<!-- END TEAM 2 NAME -->

													<!-- START MATCH DATE -->
													<div class="form-group">
														<label class="control-label col-md-4">Match Date
														<span class="required"> * </span>
														</label>
														<div class="col-md-4">
															<div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
																<input type="text" class="form-control" name="dp_date_n">
																<span class="input-group-btn">
																	<button class="btn default" type="button">
																		<i class="fa fa-calendar"></i>
																	</button>
																</span>
															</div>
														</div>
													</div>
													<!-- END MATCH DATE -->
													
													<!-- START MATCH TIME -->
													<div class="form-group">
														<label class="control-label col-md-4">Match Time
														<span class="required"> * </span>
														</label>
														<div class="col-md-4">
															<div class="input-group">
																<input type="text" class="form-control timepicker timepicker-no-seconds" name="tp_time_n">
																<span class="input-group-btn">
																	<button class="btn default" type="button">
																		<i class="fa fa-clock-o"></i>
																	</button>
																</span>
															</div>
														</div>
													</div>
													<!-- END MATCH TIME -->
													
													<div class="form-actions">
														<div class="row">
															<div class="col-md-offset-5 col-md-7">											
																<!-- Button to trigger modal -->
																<a href="#myModal1" role="button" class="btn green" data-toggle="modal"> Add </a>
															</div>
														</div>
													</div>
													<!-- Modal -->
													<div id="myModal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
																	<h4 class="modal-title">Confirm Action</h4>
																</div>
																<div class="modal-body">
																	<p> You are about to add a new NBA Match Up. Please confirm action. </p>
																</div>
																<div class="modal-footer">
																	<input type="submit" class="btn green" value="&nbsp;Confirm&nbsp;">
																</div>
															</div>
														</div>
													</div>
												
												</div>
											</form>
											
										</div>
										<!--END PBA TAB-->
									</div>
								</div>
							</div>
							<!-- END TAB PORTLET-->
						</div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
		
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2017 &copy; Online Ending by DNO Group.
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
		<script src="assets/global/plugins/respond.min.js"></script>
		<script src="assets/global/plugins/excanvas.min.js"></script> 
		<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/plugins/moment.min.js" type="text/javascript"></script>

        <script src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>

        <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
        <script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
		
		<!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
        <script src="assets/pages/scripts/form-validation.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout2/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout2/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>