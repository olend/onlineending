var FormValidation = function () {

    var handleValidation1 = function() {
        // for more info visit the official plugin documentation: 
	    // http://docs.jquery.com/Plugins/Validation
		var form1 = $('#insertTeamForm');
		var error1 = $('.alert-danger', form1);
		var success1 = $('.alert-success', form1);
			
		form1.validate({
			errorElement: 'span', //default input error message container
			errorClass: 'help-block help-block-error', // default input error message class
			focusInvalid: false, // do not focus the last invalid input
			ignore: "",  // validate all fields including form hidden input
			rules: {
				league: {
					required: true
				},
				tname: {
					required: true
				},
				image: {
					required: true
				}
			},
			messages: {
				league: "Please select a league",
				tname: "Please enter the team name",
				image: "Please select an image"
			},

			invalidHandler: function (event, validator) { //display error alert on form submit              
				success1.hide();
				error1.show();
				App.scrollTo(error1, -200);
			},

			highlight: function (element) { // hightlight error inputs
				$(element)
					.closest('.form-group').addClass('has-error'); // set error class to the control group
			},

			unhighlight: function (element) { // revert the change done by hightlight
				$(element)
					.closest('.form-group').removeClass('has-error'); // set error class to the control group
			},

			success: function (label) {
				label
					.closest('.form-group').removeClass('has-error'); // set success class to the control group
			},

			submitHandler: function (form) {
				form.submit();
				success1.show();
				error1.hide();
			}
		});
    }

    var handleWysihtml5 = function() {
        if (!jQuery().wysihtml5) {
            return;
        }

        if ($('.wysihtml5').size() > 0) {
            $('.wysihtml5').wysihtml5({
                "stylesheets": ["assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
            });
        }
    }

    return {
        //main function to initiate the module
        init: function () {
            handleWysihtml5();
            handleValidation1();
        }

    };

}();

jQuery(document).ready(function() {
    FormValidation.init();
});