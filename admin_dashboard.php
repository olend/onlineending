<!DOCTYPE html>
<?php
include('session.php');
?>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.3
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
		
		<!-- PAGE TITLE -->
        <title>Welcome,<?php echo " ".$_SESSION['uname'];?></title>
		
		
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout2/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout2/css/themes/blue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout2/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />
		<style>
			table, td {
				text-align:center;
			}
		</style>
	</head>
    <!-- END HEAD -->
	
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
	
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
			
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="javascript:;">
                        <img src="assets/layouts/layout2/img/ed_logo_a.png" alt="logo" class="logo-default"/>
					</a>
                </div>
                <!-- END LOGO -->			
				
                <!-- BEGIN PAGE ACTIONS -->
                <div class="page-actions">
                    <div class="btn-group">
                        <button type="button" class="btn btn-circle btn-outline red dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-plus"></i>&nbsp;
                            <span class="hidden-sm hidden-xs">New&nbsp;</span>&nbsp;
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="add_team.php">
                                    <i class="icon-social-dribbble"></i> Team </a>
                            </li>
                            <li>
                                <a href="add_match.php">
                                    <i class="icon-trophy"></i> Match Up </a>
                            </li>
                            <li>
                                <a href="add_admin.php">
                                    <i class="icon-support"></i> Administrator </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- END PAGE ACTIONS -->
				
                <!-- BEGIN PAGE TOP -->
                <div class="page-top">				
                     <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- BEGIN TODO DROPDOWN -->
                            <li class="dropdown dropdown-extended dropdown-tasks" id="header_task_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-calendar"></i>
                                    <span class="badge badge-default"> 3 </span>
                                </a>
                                <ul class="dropdown-menu extended tasks">
                                    <li class="external">
                                        <h3>You have
                                            <span class="bold">12 pending</span> tasks</h3>
                                        <a href="app_todo.html">view all</a>
                                    </li>
                                    <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="task">
                                                        <span class="desc">Web server upgrade</span>
                                                        <span class="percent">58%</span>
                                                    </span>
                                                    <span class="progress">
                                                        <span style="width: 58%;" class="progress-bar progress-bar-info" aria-valuenow="58" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">58% Complete</span>
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="task">
                                                        <span class="desc">Mobile development</span>
                                                        <span class="percent">85%</span>
                                                    </span>
                                                    <span class="progress">
                                                        <span style="width: 85%;" class="progress-bar progress-bar-success" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">85% Complete</span>
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="task">
                                                        <span class="desc">New UI release</span>
                                                        <span class="percent">38%</span>
                                                    </span>
                                                    <span class="progress progress-striped">
                                                        <span style="width: 38%;" class="progress-bar progress-bar-important" aria-valuenow="18" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">38% Complete</span>
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <!-- END TODO DROPDOWN -->
							
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="assets/layouts/layout2/img/avatar_la_sm.png" />
									<?php
										//RETRIEVE USER AVATAR HERE
									?>
                                    <!--<span class="username username-hide-on-mobile"> Leslie </span>-->
                                    <span class="username username-hide-on-mobile">
										<?php
											echo $_SESSION['uname'];
										?>
									</span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="admin_profile.php">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
                                    <li>
                                        <a href="app_calendar.html">
                                            <i class="icon-calendar"></i> My Calendar </a>
                                    </li>
                                    <li>
                                        <a href="logout.php">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->					
                </div>
                <!-- END PAGE TOP -->				
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
		
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
		
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <li class="nav-item start ">
                            <a href="admin_profile.php" class="nav-link nav-toggle">
                                <i class="icon-user"></i>
                                <span class="title">Profile</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-social-dribbble"></i>
                                <span class="title">Teams</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="add_team.php" class="nav-link ">
                                        <span class="title">Add</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="delete_team.php" class="nav-link ">
                                        <span class="title">Delete</span>
                                    </a>
                                </li>
							</ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-trophy"></i>
                                <span class="title">Match Ups</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="add_match.php" class="nav-link ">
                                        <span class="title">Add</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="update_match.php" class="nav-link ">
                                        <span class="title">Update</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="delete_match.php" class="nav-link ">
                                        <span class="title">Delete</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-support"></i>
                                <span class="title">Administrators</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="add_admin.php" class="nav-link ">
                                        <span class="title">Add</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="delete_admin.php" class="nav-link ">
                                        <span class="title">Delete</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="view_allMembers.php" class="nav-link nav-toggle">
                                <i class="icon-users"></i>
                                <span class="title">View Members</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="view_allLogs.php" class="nav-link nav-toggle">
                                <i class="icon-book-open"></i>
                                <span class="title">View Logs</span>
                            </a>
                        </li>
                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
			
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
				
                    <!-- BEGIN PAGE HEADER-->
					<h3 class="page-title" style="font-weight:bold; padding:0px 20px 0px 20px;"> DASHBOARD </h3>
					
					<div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <span class="caption-subject bold uppercase" style="font-size:20px"> PBA </span>
                                    </div>
                                </div>
                                <div class="portlet-body">
								<?php
									$sql = "SELECT * FROM tbl_matchup WHERE league = 'PBA' ORDER BY sched_date ASC, sched_time ASC";
									$result = mysqli_query($db, $sql);
									$row = mysqli_num_rows($result);
								?>
								<div class="scroller" style="height:150px" data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd">

									<div class="table-scrollable">
										<table class="table table-hover">
											<?php
												while($row = mysqli_fetch_array($result)) {
													$team1 = $row['team_1'];
													$team2 = $row['team_2'];
													echo '<tr>';
														echo '<td>';
															$t1 = "SELECT logo_filepath, logo_filename FROM tbl_team WHERE team_name = '$team1'";
															$r1 = mysqli_query($db, $t1);
															$o1 = mysqli_fetch_array($r1);
															$l1 = $o1['logo_filepath'].$o1['logo_filename'];
															echo '<div class="profile-userpic">';
															echo '<img src="'.$l1.'" width="70px" height="60px" />';
															echo '</div>';
														echo '</td>';

														echo '<td>';
															echo '<strong>Schedule:</strong><br>'.$row['sched_date'].'<br>'.$row['sched_time'];
														echo '</td>';

														echo '<td>';
															$t2 = "SELECT logo_filepath, logo_filename FROM tbl_team WHERE team_name = '$team2'";
															$r2 = mysqli_query($db, $t2);
															$o2 = mysqli_fetch_array($r2);
															$l2 = $o2['logo_filepath'].$o2['logo_filename'];
															echo '<div class="profile-userpic">';
															echo '<img src="'.$l2.'" width="70px" height="60px" />';
															echo '</div>';
														echo '</td>';
													echo '</tr>';
												}
											?>
										</table>
									</div>
								</div>
							</div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>
                    </div>


					<div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <span class="caption-subject bold uppercase" style="font-size:20px"> NBA </span>
                                    </div>
                                </div>
                                <div class="portlet-body">
								<?php
									$sql = "SELECT * FROM tbl_matchup WHERE league = 'NBA' ORDER BY sched_date ASC, sched_time ASC";
									$result = mysqli_query($db, $sql);
									$row = mysqli_num_rows($result);
								?>
								<div class="scroller" style="height:150px" data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd">

									<div class="table-scrollable">
										<table class="table table-hover">
											<?php
												while($row = mysqli_fetch_array($result)) {
													$team3 = $row['team_1'];
													$team4 = $row['team_2'];
													echo '<tr>';
														echo '<td>';
															$t3 = "SELECT logo_filepath, logo_filename FROM tbl_team WHERE team_name = '$team3'";
															$r3 = mysqli_query($db, $t3);
															$o3 = mysqli_fetch_array($r3);
															$l3 = $o3['logo_filepath'].$o3['logo_filename'];
															echo '<div class="profile-userpic">';
															echo '<img src="'.$l3.'" width="70px" height="60px" />';
															echo '</div>';
														echo '</td>';

														echo '<td>';
															echo '<strong>Schedule:</strong><br>'.$row['sched_date'].'<br>'.$row['sched_time'];
														echo '</td>';

														echo '<td>';
															$t4 = "SELECT logo_filepath, logo_filename FROM tbl_team WHERE team_name = '$team4'";
															$r4 = mysqli_query($db, $t4);
															$o4 = mysqli_fetch_array($r4);
															$l4 = $o4['logo_filepath'].$o4['logo_filename'];
															echo '<div class="profile-userpic">';
															echo '<img src="'.$l4.'" width="70px" height="60px" />';
															echo '</div>';
														echo '</td>';
													echo '</tr>';
												}
											?>
										</table>
									</div>
							
								</div>
							</div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>
                    </div>					
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
		
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner">
				2017 &copy; Online Ending by DNO Group.
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout2/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout2/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>