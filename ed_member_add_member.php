<!DOCTYPE html>
<?php
	include('config.php');
	session_start();
	
	if($_SERVER["REQUEST_METHOD"] == "POST") {
		$uname = mysqli_real_escape_string($db, $_POST['uname']);
		$pword = mysqli_real_escape_string($db, $_POST['pword']);
		
		$sql = "SELECT user_id, username, password, firstname, lastname, contact_number, email, user_type FROM tbl_user WHERE username = '$uname' AND password = '$pword'";
		
		$result = mysqli_query($db, $sql);
		
		$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
		$count = mysqli_num_rows($result);
		
		if($count == 1) {
			$_SESSION['user_id'] = $row['user_id'];
			$_SESSION['uname'] = $row['username'];
			$_SESSION['pword'] = $row['password'];
			$_SESSION['fname'] = $row['firstname'];
			$_SESSION['lname'] = $row['lastname'];
			$_SESSION['cnum'] = $row['contact_number'];
			$_SESSION['email'] = $row['email'];
			if($row['user_type'] == "admin") header("location: ed_admin_dashboard.php");
			else header("location: ed_member_dashboard.php");
		} else {
			echo "<h1 style='color:red;'>";
			echo "Login error.";
			echo "</h1>";
		}
	}
?>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.3
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Online Ending Game</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
	<link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
	<link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
	<link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
	<link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL STYLES -->
	<link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
	<link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<!-- END THEME GLOBAL STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link href="assets/pages/css/login.min.css" rel="stylesheet" type="text/css" />
	<!-- END PAGE LEVEL STYLES -->
	<!-- BEGIN THEME LAYOUT STYLES -->
	<!-- END THEME LAYOUT STYLES -->
	<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->

<style type="text/css">
	.logo {
		text-align: center;
	}
	.logo img{
		height:150px;
		width: 450px;
		margin-top: -60px;
		margin-bottom: -20px;
	}
	.porlet{
		float: right;
	}
	.caption1 img{
		width: 50px;
		margin-bottom: -50px;
	}
	.scroller img{
		margin-left: 20px;
	}
	.img2 {
		float: right;
		margin-top: -85px;
		margin-right: 20px;
	}
	.img3{
		float: right;
		margin-top: -70px;
		margin-right: 20px;
	}
	.img4{
		float: right;
		margin-top: -90px;
		margin-right: 20px;
		height: 100px;
		width: 100px;
	}
</style>

<body class="login">

	<!-- BEGIN LOGO -->
	<div class="logo">
		<a href="javascript:;">
		<img src="assets/layouts/layout3/img/oe-logo.png" alt="" /> </a>
	</div>
	<!-- END LOGO -->
	
	<!-- START OF RIGHT SIDE -->

	<div class="row">
		<div class="col-md-6">

			<!-- BEGIN LOGIN -->
			<div class="content" style="margin-top: -0px; width: 600px;">
				<!-- BEGIN LOGIN FORM -->
				<form class="login-form" action="insert_member.php" method="POST">
					<h3 class="font-green">Sign Up</h3>
					<p class="hint"> Enter your personal details below: </p>
					<div class="form-group">
						<label class="control-label visible-ie8 visible-ie9">First Name</label>
						<input class="form-control placeholder-no-fix" type="text" placeholder="First Name" name="fname" />
					</div>
					<div class="form-group">
						<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
						<label class="control-label visible-ie8 visible-ie9">Last Name</label>
						<input class="form-control placeholder-no-fix" type="text" placeholder="Last Name" name="lname" />
					</div>
					<div class="form-group">
						<label class="control-label visible-ie8 visible-ie9">Email Address</label>
						<input class="form-control placeholder-no-fix" type="text" placeholder="Email" name="email" />
					</div>
					<div class="form-group">
						<label class="control-label visible-ie8 visible-ie9">Contact Number</label>
						<input class="form-control placeholder-no-fix" type="text" placeholder="Contact Number" name="cnum" />
					</div>
					<p class="hint"> Enter your account details below: </p>
					<div class="form-group">
						<label class="control-label visible-ie8 visible-ie9">Username</label>
						<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="uname" />
					</div>
					<div class="form-group">
						<label class="control-label visible-ie8 visible-ie9">Password</label>
						<input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Password" name="pword" />
					</div>
					<div class="form-group">
						<label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
						<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Re-type Your Password" name="rpassword" />
					</div>
					
                    
                                                              
                                                            
					<div class="form-actions">
						<button type="button" id="register-back-btn" class="btn btn-default">Back</button>
						<button type="submit" id="register-submit-btn" class="btn btn-success uppercase pull-right">Submit</button>
					</div>
				</form>
				<!-- END LOGIN FORM -->
				<!-- BEGIN FORGOT PASSWORD FORM -->
			
				<!-- END FORGOT PASSWORD FORM -->
				<!-- BEGIN REGISTRATION FORM -->
			
				<!-- END REGISTRATION FORM -->
			</div>

		</div>

		<div class="col-md-6">
			<div class="portlet light" style="margin-right: 100px; width: 600px;">
				<div class="portlet-title tabbable-line">
					<div class="caption">
						<img src="assets/layouts/layout3/img/PBA111.png">
					</div>
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#portlet_tab1" data-toggle="tab"> MONDAY </a>
						</li>
						<li>
							<a href="#portlet_tab2" data-toggle="tab"> TUESDAY </a>
						</li>
						<li>
							<a href="#portlet_tab3" data-toggle="tab"> WEDNESDAY </a>
						</li>
					</ul>
				</div>
				<div class="portlet-body">
					<div class="tab-content">
						<!--TAB1-->
						<div class="tab-pane active" id="portlet_tab1">
							<div class="scroller" style="height: 200px;">

								<img src="assets/layouts/layout3/img/Rain-or-Shine.png">
								<div class="sched" style="text-align:center; margin-top: -50px;">
									<p>JUNE 05 / 04:15PM
									<br>
									SMART ARANETA COLISEUM</p>
								</div>
								<img src="assets/layouts/layout3/img/nlex.png" class="img2">

								<img src="assets/layouts/layout3/img/meralco.png">

								<div class="sched" style="text-align:center; margin-top: -50px;">
									<p>JUNE 05 / 07:15PM
									<br>
									SMART ARANETA COLISEUM</p>
								</div>

								<img src="assets/layouts/layout3/img/star-hotdog.png" class="img2">

							</div>
						</div>
						<!--END TAB1-->

						<!--START TAB2-->
						<div class="tab-pane" id="portlet_tab2">
							<div class="scroller" style="height:200px;">

								<img src="assets/layouts/layout3/img/sanmig.png" style="margin-top: 20px;">
								<div class="sched" style="text-align:center; margin-top: -50px;">
									<p>JUNE 06 / 04:15PM
									<br>
									SMART ARANETA COLISEUM</p>
								</div>
								<img src="assets/layouts/layout3/img/tnt.png" class="img3">

								<img src="assets/layouts/layout3/img/phoenix.png" style="margin-top: 10px;">

								<div class="sched" style="text-align:center; margin-top: -50px;">
									<p>JUNE 06 / 07:15PM
									<br>
									SMART ARANETA COLISEUM</p>
								</div>

								<img src="assets/layouts/layout3/img/star-hotdog.png" class="img2">
	
							</div>
						</div>
						<!--END TAB2-->

						<!--START TAB3-->
						<div class="tab-pane" id="portlet_tab3">
							<div class="scroller" style="height: 200px;">
								<h4>Tab 3 Content</h4>
								<p> Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent
								luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam
								erat volutpat.ut laoreet dolore magna ut laoreet dolore magna. ut laoreet dolore magna. ut laoreet dolore magna. </p>
								<p> Ut wisi enim ad m veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie
								consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
								</p>
							</div>
						</div>
						<!--END TAB3-->
					</div>
				</div>
			</div>
			<div class="portlet light" style="margin-right: 100px; width: 600px;">
				<div class="portlet-title tabbable-line">
					<div class="caption1">
						<img src="assets/layouts/layout3/img/nba1.png">
					</div>
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#portlet_tab4" data-toggle="tab"> MONDAY </a>
						</li>
						<li>
							<a href="#portlet_tab5" data-toggle="tab"> TUESDAY </a>
						</li>
						<li>
							<a href="#portlet_tab6" data-toggle="tab"> WEDNESDAY</a>
						</li>
					</ul>
				</div>
				<div class="portlet-body">
					<div class="tab-content">
						<!--START TAB4-->
						<div class="tab-pane active" id="portlet_tab4">
							<div class="scroller" style="height:170px;">

								<img src="assets/layouts/layout3/img/spurs.png" style="height: 120px; width: 120px;">
								<div class="sched" style="text-align:center; margin-top: -70px;">
									<p>JUNE 06 / 09:15AM
									<br>
									LIVE @ ABSCBN</p>
								</div>
								<img src="assets/layouts/layout3/img/warriors.png" class="img4">

							</div>
						</div>
						<!--END TAB4-->

						<!--START TAB5-->
						<div class="tab-pane" id="portlet_tab5">
							<div class="scroller" style="height:170px;">

								<img src="assets/layouts/layout3/img/cavaliers.png" style="height: 120px; width: 120px;">
								<div class="sched" style="text-align:center; margin-top: -70px;">
									<p>JUNE 06 / 09:15AM
									<br>
									LIVE @ ABSCBN</p>
								</div>
								<img src="assets/layouts/layout3/img/lakers.png" class="img4">

							</div>
						</div>
						<!--END TAB5-->

						<!--START TAB6-->
						<div class="tab-pane" id="portlet_tab6">
							<div class="scroller" style="height: 200px;">
								<h4>Tab 3 Content</h4>
								<p> Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent
								luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum 	dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam
								erat volutpat.ut laoreet dolore magna ut laoreet dolore magna. ut laoreet dolore magna. ut laoreet dolore magna. </p>
								<p> Ut wisi enim ad m veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie
								consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
								</p>
							</div>
						</div>
						<!--END TAB6-->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END OF SCHEDULE AND RECAP SIDE -->



	<!--[if lt IE 9]>
	<script src="assets/global/plugins/respond.min.js"></script>
	<script src="assets/global/plugins/excanvas.min.js"></script> 
	<![endif]-->
	<!-- BEGIN CORE PLUGINS -->
	<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
	<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
	<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
	<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
	<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
	<script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
	<script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/pages/scripts/login.min.js" type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>